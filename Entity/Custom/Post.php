<?php

namespace RusPlanet\BlogManagerBundle\Entity\Custom;

class Post
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $text;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var int
     */
    private $bloggerId;

    /**
     * @var bool
     */
    private $isActive;

    /**
     * @var integer
     */
    private $viewsCount;

    /**
     * @var string
     */
    private $image;

    /**
     * @var integer
     */
    private $commentsCount;

    /**
     * @var Blogger
     */
    private $blogger;

    /** @var User */
    private $owner;

    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
    }

    public function setUpdatedAtValue()
    {
        $this->updatedAt = new \DateTime();
    }

    public function __construct($data = null)
    {
        $this->commentsCount = 0;
        $this->viewsCount = 0;
        if (!is_null($data)) {
            $this->setUp($data);
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Post
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Post
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Post
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Post
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set viewsCount
     *
     * @param integer $viewsCount
     *
     * @return Post
     */
    public function setViewsCount($viewsCount)
    {
        $this->viewsCount = $viewsCount;

        return $this;
    }

    /**
     * Get viewsCount
     *
     * @return integer
     */
    public function getViewsCount()
    {
        return $this->viewsCount;
    }

    /**
     * Set bloggerId
     *
     * @param integer $bloggerId
     *
     * @return Post
     */
    public function setBloggerId($bloggerId)
    {
        $this->bloggerId = $bloggerId;

        return $this;
    }

    /**
     * Get bloggerId
     *
     * @return integer
     */
    public function getBloggerId()
    {
        return $this->bloggerId;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Post
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Post
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set commentsCount
     *
     * @param integer $commentsCount
     *
     * @return Post
     */
    public function setCommentsCount($commentsCount)
    {
        $this->commentsCount = $commentsCount;

        return $this;
    }

    /**
     * Get commentsCount
     *
     * @return integer
     */
    public function getCommentsCount()
    {
        return $this->commentsCount;
    }

    /**
     * Set blogger
     *
     * @param Blogger $blogger
     *
     * @return Post
     */
    public function setBlogger(Blogger $blogger = null)
    {
        $this->blogger = $blogger;

        return $this;
    }

    /**
     * Get blogger
     *
     * @return Blogger
     */
    public function getBlogger()
    {
        return $this->blogger;
    }

    /**
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param User $owner
     * @return $this
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;

        return $this;
    }

    private function setUp($data)
    {
        $this->id = $data['id'];
        $this->bloggerId = $data['blogger_id'];
        $this->title = $data['title'];
        $this->text = $data['text'];
        $this->viewsCount = $data['views_count'];
        $this->isActive = $data['is_active'];
        $this->image = $data['image'];
        $this->commentsCount = $data['comments_count'];
        $this->createdAt = new \DateTime($data['created_at']);
        $this->updatedAt = new \DateTime($data['updated_at']);

        if (isset($data['blogger']) && !is_null($data['blogger'])) {
            $this->blogger = new Blogger($data['blogger']);
        }

        if (isset($data['owner']) && !is_null($data['owner'])) {
            $this->owner = new User($data['owner']);
        }
    }

}