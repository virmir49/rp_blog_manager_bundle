<?php

namespace RusPlanet\BlogManagerBundle\Service;

use RusPlanet\BlogManagerBundle\Entity\Custom\Post;

class BlogManager
{
    const POST_GET_PATH = 'post/get';
    const POST_DELETE_PATH = 'post/delete';
    const POST_UPDATE_PATH = 'post/update';

    const COOKIE_USER_API_KEY = 'intuition_auth_token';

    protected $apiUrl;
    protected $apiVersion;

    private $authToken;
    private $postsCount;

    //TODO: Temporary solution
    protected $curlErrors = null;

    public function __construct($apiUrl, $apiVersion, $kernelRootDir)
    {
        $this->apiUrl = $apiUrl;
        $this->apiVersion = $apiVersion;
        $this->kernelRootDir = $kernelRootDir;
    }

    public function setAuthToken($authToken)
    {
        $this->authToken = $authToken;

        return $this;
    }

    /**
     * @param int $id
     * @param bool $needBlogger
     * @param bool $needOwner
     * @return Post[]
     */
    public function getPost($id, $needBlogger = false, $needOwner = false)
    {
        $params = array();
        if ($needBlogger) {
            $params['need_blogger'] = 1;
        }
        if ($needOwner) {
            $params['need_owner'] = 1;
        }

        $result = $this->sendRequest(
            implode('/', [$this->apiUrl, self::POST_GET_PATH . '/' . $id]),
            OAUTH_HTTP_METHOD_GET,
            $params,
            $this->authToken
        );

        if (isset($result['success']) && $result['success'] == true) {

            return new Post($result['result']);
        } else {

            return null;
        }
    }

    /**
     * @param bool $needBlogger
     * @param bool $needOwner
     * @param int $limit
     * @param int $offset
     * @param array $order
     * @param array $filter
     * @return array|null
     */
    public function getPosts($needBlogger = false, $needOwner = false, $limit = null, $offset = null, $order = null, $filter = null)
    {
        $params = array();
        if ($needBlogger) {
            $params['need_blogger'] = 1;
        }
        if ($needOwner) {
            $params['need_owner'] = 1;
        }
        if (!is_null($limit)) {
            $params['limit'] = $limit;
        }
        if (!is_null($offset)) {
            $params['offset'] = $offset;
        }
        if (!is_null($order)) {
            $params['order'] = $order;
        }
        if (!is_null($filter)) {
            $params['filter'] = $filter;
        }

        $result = $this->sendRequest(
            implode('/', [$this->apiUrl, self::POST_GET_PATH]),
            OAUTH_HTTP_METHOD_GET,
            $params,
            $this->authToken
        );

        if (isset($result['success']) && $result['success'] == true) {
            $data = array();
            foreach ($result['result'] as $resultEntry) {
                $data[] = new Post($resultEntry);
            }
            $this->postsCount = $result['count'];

            return $data;
        } else {

            return null;
        }
    }

    /**
     * @param int $id
     * @return array
     */
    public function deletePost($id)
    {
        $result = $this->sendRequest(
            implode('/', [$this->apiUrl, self::POST_DELETE_PATH . '/' . $id]),
            OAUTH_HTTP_METHOD_GET,
            [],
            $this->authToken
        );

        return $result;
    }

    /**
     * @param int $id
     * @param array $fields
     * @return array
     */
    public function updatePost($id, $fields = array())
    {
        $params = array();
        $params['fields'] = $fields;

        $result = $this->sendRequest(
            implode('/', [$this->apiUrl, self::POST_UPDATE_PATH . '/' . $id]),
            OAUTH_HTTP_METHOD_POST,
            $params,
            $this->authToken
        );

        return $result;
    }

    /**
     * @param string $url
     * @param string $method
     * @param array $params
     * @param string $authToken
     * @return array|bool
     */
    protected function sendRequest($url, $method, $params, $authToken = null)
    {
        $params = http_build_query($params);

        $curl = curl_init();

        if ($method == OAUTH_HTTP_METHOD_POST) {
            curl_setopt($curl, CURLOPT_POST, true);
        } else {
            $url = $url . '?' . $params;
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        if (!empty($params)) {
            curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        }

        if ($authToken) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, ["X-AUTH-TOKEN: $authToken"]);
        }

        $response = curl_exec($curl);

        $this->curlErrors = curl_error($curl);

        curl_close($curl);

        return $response ? json_decode($response, true) : false;
    }

    protected function checkValidResponse()
    {
        //TODO: Implement check functionality
    }

    /**
     * @return mixed
     */
    public function getPostsCount()
    {
        return $this->postsCount;
    }

}